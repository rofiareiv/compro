<!-- Vendor JS Files -->
  <script src="{{ asset('assets/default/vendor/bootstrap/js/bootstrap.bundle.js')}}"></script>
  <script src="{{ asset('assets/default/vendor/aos/aos.js')}}"></script>
  <script src="{{ asset('assets/default/vendor/php-email-form/validate.js')}}"></script>
  <script src="{{ asset('assets/default/vendor/swiper/swiper-bundle.min.js')}}"></script>
  <script src="{{ asset('assets/default/vendor/purecounter/purecounter.js')}}"></script>
  <script src="{{ asset('assets/default/vendor/isotope-layout/isotope.pkgd.min.js')}}"></script>
  <script src="{{ asset('assets/default/vendor/glightbox/js/glightbox.min.js')}}"></script>

  <!-- Template Main JS File -->
  <script src="{{ asset('assets/default/js/main.js')}}"></script>
