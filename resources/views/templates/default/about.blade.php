<section id="about" class="about">

      <div class="container" data-aos="fade-up">
        <div class="row gx-0">

          <div class="col-lg-6 d-flex flex-column justify-content-center" data-aos="fade-up" data-aos-delay="200">
            <div class="content">
              <h3>Who We Are</h3>
              <h2>PT. Usaha Bersama Jabar (UBJ) adalah anak perusahaan PT. Jasa Sarana, salah satu BUMD Pemprov Jabar.</h2>
              <p>
                UBJ menjalankan usahanya dalam bidang perdagangan umum, alat laboratorium, farmasi dan kesehatan, transporter, dan warehousing. Sesuai akte nomor 04 tahun 2020 tanggal 25 September 2020.
              </p>
              <div class="text-center text-lg-start">
                <a href="{{ route('about-detail') }}" class="btn-read-more d-inline-flex align-items-center justify-content-center align-self-center">
                  <span>Selengkapnya</span>
                  <i class="bi bi-arrow-right"></i>
                </a>
              </div>
            </div>
          </div>

          <div class="col-lg-6 d-flex align-items-center" data-aos="zoom-out" data-aos-delay="200">
            <img src="{{ asset('assets/default/img/about.jpg')}}" class="img-fluid" alt="">
          </div>

        </div>
      </div>

    </section>
