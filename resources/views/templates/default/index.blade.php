<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

@include('templates.default.component.header')

<body>

  <!-- ======= Header ======= -->
  @include('templates.default.component.nav-header')
  <!-- End Header -->

  @yield('content')

  <!-- ======= Footer ======= -->
  @include('templates.default.component.nav-footer')
  <!-- End Footer -->

  <a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i class="bi bi-arrow-up-short"></i></a>

  @include('templates.default.component.footer')

</body>

</html>
