  @extends('templates.default.index')

  @section('content')

  <!-- ======= Hero Section ======= -->
  @include('templates.default.hero')
  <!-- End Hero -->

  <main id="main">
    <!-- ======= About Section ======= -->
    @include('templates.default.about')
    <!-- End About Section -->

    <!-- ======= Values Section ======= -->
    @include('templates.default.values')
    <!-- End Values Section -->

    <!-- ======= Counts Section ======= -->
    @include('templates.default.counts')
    <!-- End Counts Section -->

    <!-- ======= Features Section ======= -->
    {{-- @include('templates.default.feature') --}}
    <!-- End Features Section -->

    <!-- ======= Services Section ======= -->
    @include('templates.default.service')
    <!-- End Services Section -->

    <!-- ======= Pricing Section ======= -->
    @include('templates.default.pricing')
    <!-- End Pricing Section -->

    <!-- ======= F.A.Q Section ======= -->
    @include('templates.default.faq')
    <!-- End F.A.Q Section -->

    <!-- ======= Portfolio Section ======= -->
    @include('templates.default.portfolio')
    <!-- End Portfolio Section -->

    <!-- ======= Testimonials Section ======= -->
    @include('templates.default.testimoni')
    <!-- End Testimonials Section -->

    <!-- ======= Team Section ======= -->
    @include('templates.default.team')
    <!-- End Team Section -->

    <!-- ======= Clients Section ======= -->
    @include('templates.default.client')
    <!-- End Clients Section -->

    <!-- ======= Recent Blog Posts Section ======= -->
    @include('templates.default.blog')
    <!-- End Recent Blog Posts Section -->

    <!-- ======= Contact Section ======= -->
    @include('templates.default.contact')
    <!-- End Contact Section -->

  </main>
  <!-- End #main -->

@endsection
