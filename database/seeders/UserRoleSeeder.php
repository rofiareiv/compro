<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;

class UserRoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Buat data dummy disini
        Role::create([
            'name' => 'Admin',
            'guard_name' => 'admin'
        ]);

        Role::create([
            'name' => 'Staff',
            'guard_name' => 'staff'
        ]);
    }
}
