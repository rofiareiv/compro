<?php

namespace Database\Seeders;

use App\Models\SystemTemplate;
use Illuminate\Database\Seeder;

class SystemTemplateSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        SystemTemplate::create([
            'name' => 'Flexstart',
            'slug' => 'flexstart',
            'is_active' => 1,
            'created_by' => 1,
        ]);
    }
}
