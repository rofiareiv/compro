<?php

namespace Database\Seeders;

use App\Models\ComproAbout;
use Illuminate\Database\Seeder;

class ComproAboutSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ComproAbout::create([
            'title' => 'Dynamict Web Company Profile',
            'content' => 'Merupakan website company profile dinamis. Anda dapat melakukan banyak perubahan termasuk template.',
            'thumbnail' => 'default.jpg',
            'created_by' => 1,
        ]);
    }
}
