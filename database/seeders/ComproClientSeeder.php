<?php

namespace Database\Seeders;

use App\Models\ComproClient;
use Illuminate\Database\Seeder;

class ComproClientSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ComproClient::create([
            'name' => 'Testing',
            'image' => 'test.jpg',
            'website' => 'website.com',
            'created_by' => 1,
        ]);
    }
}
