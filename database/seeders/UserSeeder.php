<?php

namespace Database\Seeders;

use App\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $role = Role::all();

        // $permissions = Permission::pluck('id','id')->all();

        // $role->syncPermissions($permissions);

        // buat data dummy disini
        $admin = User::create([
            'first_name' => 'Sysadmin',
            'last_name' => 'Admin',
            'email' => 'sysadmin@compro.test',
            'email_verified_at' => Carbon::now(),
            'mobile_phone' => '123456789',
            'is_admin' => true,
            'is_active' => 1,
            'password' => bcrypt(12345678),
            'avatar' => 'face15.jpg',
        ]);

        $admin->assignRole('Admin');

        $staff = User::create([
            'first_name' => 'Staff',
            'last_name' => 'Admin',
            'email' => 'staff@compro.test',
            'email_verified_at' => Carbon::now(),
            'mobile_phone' => '123456789',
            'is_admin' => true,
            'is_active' => 1,
            'password' => bcrypt(12345678),
            'avatar' => 'face15.jpg',
        ]);

        $staff->assignRole('Staff');
    }
}
