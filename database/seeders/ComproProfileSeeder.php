<?php

namespace Database\Seeders;

use App\Models\ComproProfile;
use Illuminate\Database\Seeder;

class ComproProfileSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ComproProfile::create([
            'name' => 'Bareng Saya',
            'nick_name' => 'BS',
            'address' => 'Pamekasan Madura',
            'phone' => '+62 851 5535 5774',
            'fax' => '+62 851 5535 5774',
            'email' => 'info.barengsaya@gmail.com',
            'created_by' => 1,
        ]);
    }
}
