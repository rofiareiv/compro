<?php

use App\Http\Controllers\ComproAboutController;
use App\Http\Controllers\ComproClientController;
use App\Http\Controllers\ComproPortfolioController;
use App\Http\Controllers\ComproProfileController;
use App\Http\Controllers\ComproTeamController;
use App\Http\Controllers\DashHomeController;
use App\Http\Controllers\FrotEndController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [FrotEndController::class, 'index'])->name('home');
Route::get('/page/about/detail', [FrotEndController::class, 'about'])->name('about-detail');
Route::get('/page/portfolio/detail', [FrotEndController::class, 'portfolio'])->name('portfolio-detail');
Route::get('/page/blogs', [FrotEndController::class, 'blogs'])->name('blogs');
Route::get('/page/blog/detail', [FrotEndController::class, 'blog'])->name('blog-detail');

/**
 * Routing untuk halaman admin
 */
Route::prefix('bspanel')->group( function() {
    Route::get('/', [DashHomeController::class, 'index'])->name('dash');
    Route::get('/profile', [ComproProfileController::class, 'index'])->name('profile');
    Route::get('/about', [ComproAboutController::class, 'index'])->name('about');
    Route::get('/client', [ComproClientController::class, 'index'])->name('client');
    Route::get('/team', [ComproTeamController::class, 'index'])->name('team');
    Route::get('/portfolio', [ComproPortfolioController::class, 'index'])->name('portfolio');
});
