<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ComproProfile extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $fillable = [
        'name',
        'nick_name',
        'address',
        'phone',
        'fax',
        'email',
        'district_id',
        'meta_description',
        'meta_keyword',
    ];

    protected $hidden = [
        'created_by',
    ];
}
