<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ComproSosmed extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = [
        'icon',
        'name',
        'url'
    ];

    protected $hidden = [
        'created_by',
    ];
}
