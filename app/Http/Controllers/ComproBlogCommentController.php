<?php

namespace App\Http\Controllers;

use App\Models\ComproBlogComment;
use Illuminate\Http\Request;

class ComproBlogCommentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ComproBlogComment  $comproBlogComment
     * @return \Illuminate\Http\Response
     */
    public function show(ComproBlogComment $comproBlogComment)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\ComproBlogComment  $comproBlogComment
     * @return \Illuminate\Http\Response
     */
    public function edit(ComproBlogComment $comproBlogComment)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\ComproBlogComment  $comproBlogComment
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ComproBlogComment $comproBlogComment)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ComproBlogComment  $comproBlogComment
     * @return \Illuminate\Http\Response
     */
    public function destroy(ComproBlogComment $comproBlogComment)
    {
        //
    }
}
