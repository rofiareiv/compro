<?php

namespace App\Http\Controllers;

use App\Models\ComproBlogCategory;
use Illuminate\Http\Request;

class ComproBlogCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ComproBlogCategory  $comproBlogCategory
     * @return \Illuminate\Http\Response
     */
    public function show(ComproBlogCategory $comproBlogCategory)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\ComproBlogCategory  $comproBlogCategory
     * @return \Illuminate\Http\Response
     */
    public function edit(ComproBlogCategory $comproBlogCategory)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\ComproBlogCategory  $comproBlogCategory
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ComproBlogCategory $comproBlogCategory)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ComproBlogCategory  $comproBlogCategory
     * @return \Illuminate\Http\Response
     */
    public function destroy(ComproBlogCategory $comproBlogCategory)
    {
        //
    }
}
