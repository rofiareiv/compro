<?php

namespace App\Http\Controllers;

use App\Models\SystemTemplate;
use Illuminate\Http\Request;

class SystemTemplateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\SystemTemplate  $systemTemplate
     * @return \Illuminate\Http\Response
     */
    public function show(SystemTemplate $systemTemplate)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\SystemTemplate  $systemTemplate
     * @return \Illuminate\Http\Response
     */
    public function edit(SystemTemplate $systemTemplate)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\SystemTemplate  $systemTemplate
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SystemTemplate $systemTemplate)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\SystemTemplate  $systemTemplate
     * @return \Illuminate\Http\Response
     */
    public function destroy(SystemTemplate $systemTemplate)
    {
        //
    }
}
