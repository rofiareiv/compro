<?php

namespace App\Http\Controllers;

use App\Models\ComproTag;
use Illuminate\Http\Request;

class ComproTagController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ComproTag  $comproTag
     * @return \Illuminate\Http\Response
     */
    public function show(ComproTag $comproTag)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\ComproTag  $comproTag
     * @return \Illuminate\Http\Response
     */
    public function edit(ComproTag $comproTag)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\ComproTag  $comproTag
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ComproTag $comproTag)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ComproTag  $comproTag
     * @return \Illuminate\Http\Response
     */
    public function destroy(ComproTag $comproTag)
    {
        //
    }
}
