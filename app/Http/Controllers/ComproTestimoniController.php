<?php

namespace App\Http\Controllers;

use App\Models\ComproTestimoni;
use Illuminate\Http\Request;

class ComproTestimoniController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ComproTestimoni  $comproTestimoni
     * @return \Illuminate\Http\Response
     */
    public function show(ComproTestimoni $comproTestimoni)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\ComproTestimoni  $comproTestimoni
     * @return \Illuminate\Http\Response
     */
    public function edit(ComproTestimoni $comproTestimoni)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\ComproTestimoni  $comproTestimoni
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ComproTestimoni $comproTestimoni)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ComproTestimoni  $comproTestimoni
     * @return \Illuminate\Http\Response
     */
    public function destroy(ComproTestimoni $comproTestimoni)
    {
        //
    }
}
