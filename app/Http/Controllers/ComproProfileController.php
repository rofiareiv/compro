<?php

namespace App\Http\Controllers;

use App\Models\ComproProfile;
use Illuminate\Http\Request;

class ComproProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.pages.profile.profile');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ComproProfile  $comproProfile
     * @return \Illuminate\Http\Response
     */
    public function show(ComproProfile $comproProfile)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\ComproProfile  $comproProfile
     * @return \Illuminate\Http\Response
     */
    public function edit(ComproProfile $comproProfile)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\ComproProfile  $comproProfile
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ComproProfile $comproProfile)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ComproProfile  $comproProfile
     * @return \Illuminate\Http\Response
     */
    public function destroy(ComproProfile $comproProfile)
    {
        //
    }
}
