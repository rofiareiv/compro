<?php

namespace App\Http\Controllers;

use App\Models\ComproSosmed;
use Illuminate\Http\Request;

class ComproSosmedController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ComproSosmed  $comproSosmed
     * @return \Illuminate\Http\Response
     */
    public function show(ComproSosmed $comproSosmed)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\ComproSosmed  $comproSosmed
     * @return \Illuminate\Http\Response
     */
    public function edit(ComproSosmed $comproSosmed)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\ComproSosmed  $comproSosmed
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ComproSosmed $comproSosmed)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ComproSosmed  $comproSosmed
     * @return \Illuminate\Http\Response
     */
    public function destroy(ComproSosmed $comproSosmed)
    {
        //
    }
}
