<?php

namespace App\Http\Controllers;

use App\Models\ComproPortfolio;
use Illuminate\Http\Request;

class ComproPortfolioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.pages.portfolio.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ComproPortfolio  $comproPortfolio
     * @return \Illuminate\Http\Response
     */
    public function show(ComproPortfolio $comproPortfolio)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\ComproPortfolio  $comproPortfolio
     * @return \Illuminate\Http\Response
     */
    public function edit(ComproPortfolio $comproPortfolio)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\ComproPortfolio  $comproPortfolio
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ComproPortfolio $comproPortfolio)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ComproPortfolio  $comproPortfolio
     * @return \Illuminate\Http\Response
     */
    public function destroy(ComproPortfolio $comproPortfolio)
    {
        //
    }
}
