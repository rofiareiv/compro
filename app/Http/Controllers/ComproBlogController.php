<?php

namespace App\Http\Controllers;

use App\Models\ComproBlog;
use Illuminate\Http\Request;

class ComproBlogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ComproBlog  $comproBlog
     * @return \Illuminate\Http\Response
     */
    public function show(ComproBlog $comproBlog)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\ComproBlog  $comproBlog
     * @return \Illuminate\Http\Response
     */
    public function edit(ComproBlog $comproBlog)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\ComproBlog  $comproBlog
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ComproBlog $comproBlog)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ComproBlog  $comproBlog
     * @return \Illuminate\Http\Response
     */
    public function destroy(ComproBlog $comproBlog)
    {
        //
    }
}
