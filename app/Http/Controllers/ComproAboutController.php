<?php

namespace App\Http\Controllers;

use App\Models\ComproAbout;
use Illuminate\Http\Request;

class ComproAboutController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.pages.about.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ComproAbout  $comproAbout
     * @return \Illuminate\Http\Response
     */
    public function show(ComproAbout $comproAbout)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\ComproAbout  $comproAbout
     * @return \Illuminate\Http\Response
     */
    public function edit(ComproAbout $comproAbout)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\ComproAbout  $comproAbout
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ComproAbout $comproAbout)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ComproAbout  $comproAbout
     * @return \Illuminate\Http\Response
     */
    public function destroy(ComproAbout $comproAbout)
    {
        //
    }
}
