<?php

namespace App\Http\Controllers;

use App\Models\ComproTeam;
use Illuminate\Http\Request;

class ComproTeamController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.pages.team.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ComproTeam  $comproTeam
     * @return \Illuminate\Http\Response
     */
    public function show(ComproTeam $comproTeam)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\ComproTeam  $comproTeam
     * @return \Illuminate\Http\Response
     */
    public function edit(ComproTeam $comproTeam)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\ComproTeam  $comproTeam
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ComproTeam $comproTeam)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ComproTeam  $comproTeam
     * @return \Illuminate\Http\Response
     */
    public function destroy(ComproTeam $comproTeam)
    {
        //
    }
}
