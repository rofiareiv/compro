<?php

namespace App\Providers;

use App\Models\ComproProfile;
use Illuminate\Support\ServiceProvider;

class ProfileServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        view()->composer('*', function($view) {
            $profile = ComproProfile::first();

            return $view->with(['profile' => $profile]);
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
